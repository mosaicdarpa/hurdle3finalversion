package com.uantwerpen.SC2.MDP;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class TransitionModel implements MDP{
	private double[][][] transitionMatrix;
	private double [][] actionStateCounter;
	private int numStates;
	private int numActions;
	private List<List<List<Double>>> TM;
	private Random r;
	/**
	 * @param matrix
	 */
	public TransitionModel(List<List<List<Double>>> transitionModel){
		super();
		this.TM = transitionModel;
		this.numStates=transitionModel.size();
		this.numActions=transitionModel.get(0).size();
		this.transitionMatrix= new double[numStates][numActions][numStates];
		for (int i=0; i<numStates;i++){
			for (int j=0; j<numStates;j++){
				for (int k=0; k<numStates;k++){
					transitionMatrix[i][j][k]=TM.get(i).get(j).get(k);
				}
			}
			
		}
	}

	public double getTransitionProbability(int state, int action, int nextState){
		return transitionMatrix[state][action][nextState];
	}
	
	public void setSeed(long seed){
		r = new Random(seed);
	}
	
	
	public int getNextState(int state, int action){
		int resp = 0;
		double p = r.nextDouble();
		double cumulativeProbability = 0.0;
		//System.out.println("TM "+p);
		for (int i=0; i<numStates; i++) {
			cumulativeProbability += getTransitionProbability(state,action,i);
		    if (p <= cumulativeProbability) {
		    	resp=i;
		        break;
		    }
		}
		return resp;
	}
	
	public void printTransitionPr(int state){
		for (int i=0; i<numActions;i++){
			//double value = 0;
			for (int j=0; j<numStates;j++){
				System.out.printf("%.3f ", getTransitionProbability(state, i, j));
				//value+=getTransitionProbaility(s.toNumber(), i, j);	
				}
			System.out.println();
			//System.out.println(value);
		}
		System.out.println();
	}
	
	public ArrayList<Double> getProbabilitiesSS(int state, int next_state){
		ArrayList<Double> result = new ArrayList<Double>();
		for (int i=0;i<numActions; i++){
			result.add(getTransitionProbability(state, i, next_state));
		}
		return result;
	}
	
	public ArrayList<Double> getProbabilitiesSA(int state, int next_state){
		ArrayList<Double> result = new ArrayList<Double>();
		for (int i=0;i<numActions; i++){
			result.add(getTransitionProbability(state, i, next_state));
		}
		return result;
	}

	public void printSACounter() {
		for (int i=0; i<numActions; i++){
			for (int j=0; j<numActions; j++){
				System.out.printf("%.1f ",actionStateCounter[i][j]);
			}
			System.out.println();
		}
	}
	
	public int getNumActions(){
		return numActions;
	}
	
	public int getNumStates(){
		return numActions;
	}

	@Override
	public int getReward(int state, int action, int next_state) {
		// TODO Auto-generated method stub
		return 0;
	}
}
