package com.uantwerpen.SC2.MDP;

import java.util.ArrayList;


public interface MDP {
	public double getTransitionProbability(int state, int action, int next_state);

	public int getReward(int state, int action, int next_state);
	
	public int getNextState(int state, int action);
	public void setSeed(long seed);
	
	public void printTransitionPr(int state);
	
	public ArrayList<Double> getProbabilitiesSS(int state, int next_state);
	
	public ArrayList<Double> getProbabilitiesSA(int state, int next_state);
	
	public int getNumActions();
	
	public int getNumStates();
	

}
