package com.uantwerpen.SC2.MDP;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class DynamicMDPModel implements MDP{
	private double[][][] transitionMatrix;
	private double [][] actionStateCounter;
	private int numStates;
	private int numActions;
	List<List<List<Double>>> TM;
	private Random r;
	/**
	 * @param matrix
	 */
	public DynamicMDPModel(int numStates, int numActions) {
		super();
		this.numStates=numStates;
		this.numActions=numActions;
		this.transitionMatrix= new double[numStates][numActions][numStates];
		this.actionStateCounter= new double[numStates][numActions];
	}

	public DynamicMDPModel(List<List<List<Double>>> TM) {
		super();
		this.TM = TM;
		this.numStates=TM.size();
		this.numActions=TM.get(0).size();
		this.transitionMatrix= new double[numStates][numActions][numStates];
		this.actionStateCounter= new double[numStates][numActions];
		for (int i=0; i<TM.size();i++){
			for (int j=0; j<TM.size();j++){
				for (int k=0; k<TM.size();k++){
					transitionMatrix[i][j][k]=TM.get(i).get(j).get(k);
				}
			}
			
		}
	}
	
	
	public void updateTransition(int state, int action, int nextState){
		transitionMatrix[state][action][nextState]+=1;	
		actionStateCounter[state][action]+=1;
	}
	
	public double getTransitionProbability(int state, int action, int nextState){
		if (actionStateCounter[state][action]==0.0d) return 0;
		else return transitionMatrix[state][action][nextState]/actionStateCounter[state][action];
	}
	
	public int getRewardSC(int state, int sc, int nextState){
		if (sc==nextState) return -12;
		else return 1;
	}
	
	public int getRewardGC(int state, int gc, int nextState){
		if (gc==nextState) return 3;
		else return 0;
	}
	
	public void setSeed(long seed){
		r = new Random(seed);
	}
	
	public int getNextState(int state, int action){
		int resp = 0;
		double p = r.nextDouble();
		double cumulativeProbability = 0.0;
		//System.out.println("DM "+p);
		for (int i=0; i<numStates; i++) {
			cumulativeProbability += getTransitionProbability(state,action,i);
		    if (p <= cumulativeProbability) {
		    	resp=i;
		        break;
		    }
		}
		return resp;
	}
	
	public void printTransitionPr(int state){
		for (int i=0; i<numActions;i++){
			//double value = 0;
			for (int j=0; j<numStates;j++){
				System.out.printf("%.3f ", getTransitionProbability(state, i, j));
				//value+=getTransitionProbaility(s.toNumber(), i, j);	
				}
			System.out.println();
			//System.out.println(value);
		}
		System.out.println();
	}
	
	public ArrayList<Double> getProbabilitiesSS(int state, int next_state){
		ArrayList<Double> result = new ArrayList<Double>();
		for (int i=0;i<numActions; i++){
			result.add(getTransitionProbability(state, i, next_state));
		}
		return result;
	}
	
	public ArrayList<Double> getProbabilitiesSA(int state, int next_state){
		ArrayList<Double> result = new ArrayList<Double>();
		for (int i=0;i<numActions; i++){
			result.add(getTransitionProbability(state, i, next_state));
		}
		return result;
	}

	public void printSACounter() {
		for (int i=0; i<numActions; i++){
			for (int j=0; j<numActions; j++){
				System.out.printf("%.1f ",actionStateCounter[i][j]);
			}
			System.out.println();
		}
	}
	
	public int getNumActions(){
		return numActions;
	}
	
	public int getNumStates(){
		return numActions;
	}

	@Override
	public int getReward(int state, int action, int next_state) {
		return 0;
	}
	
	public List<List<List<Double>>> asArray(){
		List<List<List<Double>>> tm = new ArrayList<List<List<Double>>>();
		for (int i=0; i<numStates;i++){
			List<List<Double>> actionList = new ArrayList<List<Double>>();
			for (int j=0; j<numActions;j++){
				List<Double> nextStateList = new ArrayList<Double>();
				for (int k=0; k<numStates;k++){
					nextStateList.add(getTransitionProbability(i,j,k));
				}
				actionList.add(nextStateList);
			}
			tm.add(actionList);
		}
		
	return tm;
	}
}
