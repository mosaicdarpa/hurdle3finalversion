package com.uantwerpen.SC2.hurdle3.utils;

import java.util.List;
import java.util.Random;

import com.uantwerpen.SC2.MDP.MDP;
import com.uantwerpen.SC2.hurdle3.SolutionHandler;

public class MDPChecker {
	
	
	public static boolean checkMDP(MDP model){
		boolean correct=false;
		double tempV=0.0d;
		for (int i=0; i<SolutionHandler.num_states;i++){
			for (int j=0; j<SolutionHandler.num_states;j++){
				for (int k=0; k<SolutionHandler.num_states;k++){
					tempV+= model.getTransitionProbability(i, j, k);
					if (tempV>1.000000001d&&tempV<0.99999999999d) 
						return correct;
				}
				tempV=0;
			}
		}
		correct=true;		
		return correct;
	}
	
	public static double comparePrecision(MDP model1, MDP model2, int numTests) {
		long error = 0;
		Random r = new Random(123456789);
		long seed = 987654321;
		model1.setSeed(seed);
		model2.setSeed(seed);
		int state = r.nextInt(model1.getNumStates());
		int action = r.nextInt(model1.getNumActions());
		for (int i=0; i<numTests;i++){
			if (model1.getNextState(state, action)!=model2.getNextState(state, action)) error+= 1;
		}
		return (error/(double) numTests);
			
	}
	public static boolean checkMatrix (List<List<List<Double>>> transitionMatrix){
		boolean correct=false;
		double tempV=0.0d;
		for (int i=0; i<SolutionHandler.num_states;i++){
			for (int j=0; j<SolutionHandler.num_states;j++){
				for (int k=0; k<SolutionHandler.num_states;k++){
					tempV+= transitionMatrix.get(i).get(j).get(k);
					if (tempV>1.000000001d&&tempV<0.99999999999d) 
						return correct;
				}
				tempV=0;
			}
		}
		correct=true;		
		return correct;
	}

}
