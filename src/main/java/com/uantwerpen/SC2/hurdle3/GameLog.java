package com.uantwerpen.SC2.hurdle3;

import java.util.ArrayList;
import java.util.function.IntPredicate;
import java.util.stream.IntStream;

public class GameLog {

	ArrayList<LogEntry> logs;
	
	public GameLog() {
		logs = new ArrayList<>(30000);
	}
	
	public void addLog(LogEntry log){
		logs.add(log);
	}
	
	public int size(){
		return logs.size();
	}
	
	public LogEntry getLog(int step){
		return logs.get(step);
	}
	
	public int collisionAvoided(int start, int end){
		return (int)IntStream.range(start, end).parallel().filter(new IntPredicate() {
			
			@Override
			public boolean test(int value) {
				return logs.get(value).collisionAvoided();
			}
		}).count();
	}

	public int collisionAvoided(int start){
		return collisionAvoided(start, logs.size());
	}
	
	public int collisionAvoided(){
		return collisionAvoided(0, logs.size());
	}
	
	public int collisions(int start, int end){
		return (end-start)-collisionAvoided(start, end);
	}
	
	public int collisions(int start){
		return collisions(start, logs.size());
	}
	
	public int collisions(){
		return collisions(0, logs.size());
	}
	
	public int correctPredictions(int start, int end){
		return (int)IntStream.range(start, end).parallel().filter(new IntPredicate() {
			
			@Override
			public boolean test(int value) {
				return logs.get(value).correctPrediction();
			}
		}).count();
	}
	
	public int correctPredictions(int start){
		return correctPredictions(start, logs.size());
	}
	
	public int correctPredictions(){
		return correctPredictions(0, logs.size());
	}

	@Override
	public String toString() {
		return  logs.toString();
	}
	
}
