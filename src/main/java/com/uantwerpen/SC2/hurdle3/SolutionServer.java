package com.uantwerpen.SC2.hurdle3;

import java.net.InetSocketAddress;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.thrift.server.TServer;
import org.apache.thrift.server.TSimpleServer;
import org.apache.thrift.transport.TServerSocket;
import org.apache.thrift.transport.TTransportException;

import com.uantwerpen.SC2.hurdle3.agent.ValueIterationAgent;
import com.uantwerpen.SC2.hurdle3.thrift.Hurdle3Execution;
import com.uantwerpen.SC2.hurdle3.thrift.Hurdle3Execution.Processor;

public class SolutionServer implements Runnable {

	private SolutionHandler								handler;
	private Hurdle3Execution.Processor<SolutionHandler>	processor;
	private TServerSocket 								socket;
	
	private final static String		name				= "SolutionServer";
	private final static String		defaultHost 		= "0.0.0.0";
	private final static int		defaultPort			= 9090;
	
	public static CommandLine cmd = null;

	public SolutionServer(SolutionHandler handler, Processor<SolutionHandler> processor, TServerSocket socket) {
		super();
		this.handler = handler;
		this.processor = processor;
		this.socket = socket;
	}

	public void run() {
		try{
			TServer server = new TSimpleServer(new TServer.Args(socket).processor(processor));
			System.out.println("Starting the server...");
			handler.setServer(server);
			server.serve();
			System.out.println("Closing the server...");
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}

	public static void main(String[] args) {
		Options options = new Options();
		options.addOption(Option.builder("h")
							.longOpt("help")
							.hasArg(false)
							.desc("Show help message")
							.build());
		options.addOption(Option.builder()
							.longOpt("host")
							.desc("IP address that the solution server will listen for connections on")
							.hasArg(true)
							.build());
		options.addOption(Option.builder()
							.longOpt("rpc-port")
							.desc("Port for RPC connections")
							.hasArg(true)
							.build());
//		options.addOption(Option.builder("a")
//							.longOpt("agent")
//							.desc("Name of agent that for this solution server")
//							.hasArg(true)
//							.required(true)
//							.build());
		options.addOption(Option.builder("m")
							.longOpt("states")
							.desc("Number of channels/states used")
							.hasArg(true)
							.build());
		
		try{
			CommandLineParser parser = new DefaultParser();
			cmd = parser.parse(options, args);
			
			if(cmd.hasOption('h')){
				printHelp(name, options);
				return;
			}
			
			SolutionHandler.num_states = Integer.valueOf(cmd.getOptionValue('m', String.valueOf(SolutionHandler.num_states)));
			
			SolutionHandler handler = new SolutionHandler(new ValueIterationAgent());
			Hurdle3Execution.Processor<SolutionHandler>	processor = new Hurdle3Execution.Processor<SolutionHandler>(handler);
			TServerSocket socket = new TServerSocket(new InetSocketAddress(
														cmd.getOptionValue("host", defaultHost), 
														Integer.valueOf(cmd.getOptionValue("rpc-port", String.valueOf(defaultPort)))));
			SolutionServer ss = new SolutionServer(handler, processor, socket);
			new Thread(ss).start();
			
		}catch(ParseException e){
			System.err.println(e.getMessage());
			printHelp(name, options);
		}catch(TTransportException e){
			System.err.println(e.getMessage());
		}catch(Exception e){
			System.err.println(e.getMessage());
			e.printStackTrace();
		}
		
	}
	
	private static void printHelp(String name, Options options){
		HelpFormatter formatter = new HelpFormatter();
		formatter.printHelp(name, options, true);
	}

}
