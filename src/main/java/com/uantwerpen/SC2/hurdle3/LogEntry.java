package com.uantwerpen.SC2.hurdle3;

public class LogEntry {
	
	private int darpa_state;
	private int agent_state;
	private int prediction_darpa;
	
	
	public LogEntry(int darpa_state, int agent_state, int prediction_darpa) {
		super();
		this.darpa_state = darpa_state;
		this.agent_state = agent_state;
		this.prediction_darpa = prediction_darpa;
	}

	public final int getDarpa_state() {
		return darpa_state;
	}


	public final int getAgent_state() {
		return agent_state;
	}


	public final int getPrediction_darpa() {
		return prediction_darpa;
	}

	public final boolean collisionAvoided(){
		return darpa_state != agent_state;
	}
	
	public final boolean correctPrediction(){
		return darpa_state == prediction_darpa;
	}

	@Override
	public String toString() {
		return "LogEntry [darpa_state=" + darpa_state + ", agent_state=" + agent_state + ", prediction_darpa="
				+ prediction_darpa + "]";
	}

}
