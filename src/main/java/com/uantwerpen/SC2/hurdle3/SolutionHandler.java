package com.uantwerpen.SC2.hurdle3;

import org.apache.thrift.TException;
import org.apache.thrift.server.TServer;

import com.uantwerpen.SC2.hurdle3.thrift.Hurdle3Execution;
import com.uantwerpen.SC2.hurdle3.thrift.StepResult;

public class SolutionHandler implements Hurdle3Execution.Iface {

	//Objects to solve the problem
	private Solution 			solution;
	private TServer				server 		= null;
	
	//Objects to help and log
	private GameLog				logs		= null;
	private int					step = 0;
	private StepResult			lastResult 	= null;
	private int					gameNr = -1;
	
	public final static int	no_collision_reward 		= 1;
	public final static int	collision_penalty			= -12;
	public final static int	prediction_success_reward	= 3;
	
	//Global constants
	public static int num_states 	= 10;
	public static int learningSteps = 29000;
	
	public SolutionHandler(Solution solution){
		this.solution 	= solution;
	}
	
	public void setServer(TServer server){
		this.server = server;
	}

	public StepResult start() throws TException {
		printStatistics();
		
		System.out.println("Start game " + String.valueOf(++gameNr));
		step = 0;
		logs = new GameLog();
		lastResult = solution.start();
		System.gc(); 					//RUN GARBAGE COLLECTOR (Big tests improvement)
		return lastResult;
	}

	public StepResult step(int reward, int observation) throws TException {
		logs.addLog(new LogEntry(observation, lastResult.next_state, lastResult.predicted_state));
		lastResult = solution.step(reward, observation, ++step, lastResult);
		return lastResult;
	}

	public void stop() throws TException {
		printStatistics();
		if(server == null){
			throw new TException("Could not stop server...");
		}
		server.stop();
	}
	
	public void printStatistics(){
		if(lastResult != null){

			final String onlyNumber = "%-26s: %-5d\n";
			final String numberAndRatio = "%-26s: %-5d (%5.2f%%)\n";
			
			int avoidedCollisions = logs.collisionAvoided();
			int collisions = logs.collisions();
			int correctPredictions = logs.correctPredictions();
			
			System.out.format(onlyNumber, "Size of logs", logs.size());
			System.out.format(numberAndRatio, "Total avoided collisions", avoidedCollisions, toPersentages(avoidedCollisions, logs.size()));
			System.out.format(numberAndRatio, "Total collisions", collisions, toPersentages(collisions, logs.size()));
			System.out.format(numberAndRatio, "Total correct predictions", correctPredictions, toPersentages(correctPredictions, logs.size()));
			
			avoidedCollisions = logs.collisionAvoided(learningSteps);
			collisions = logs.collisions(learningSteps);
			correctPredictions = logs.correctPredictions(learningSteps);
			int steps = logs.size() - learningSteps;
			
			System.out.format(numberAndRatio, "Avoided collisions (last)", avoidedCollisions, toPersentages(avoidedCollisions, steps));
			System.out.format(numberAndRatio, "Collisions (last)", collisions, toPersentages(collisions, steps));
			System.out.format(numberAndRatio, "Correct predictions (last)", correctPredictions, toPersentages(correctPredictions, steps));
			System.out.println("");
		}
	}
	
	private double toPersentages(int number, int size){
		return (double)number/size*100;
	}
	
}
