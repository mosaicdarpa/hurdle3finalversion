package com.uantwerpen.SC2.hurdle3;

import com.uantwerpen.SC2.hurdle3.thrift.StepResult;

public interface Solution {

	/**
	 * Start (or restart) the agent and give a first guess
	 * @return First Step Result
	 */
	public StepResult start();
	
	/**
	 * Next step
	 * @param reward reward of precious action
	 * @param observation channel used by darpa
	 * @param stepNr step number
	 * @return Step result for the next step
	 */
	public StepResult step(int reward, int observation, int stepNr, StepResult previousStep);
	
}
