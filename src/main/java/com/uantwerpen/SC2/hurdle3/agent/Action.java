package com.uantwerpen.SC2.hurdle3.agent;

public class Action implements com.uantwerpen.valueIteration.Action {

	private int next_state;
	private int prediction;
	
	public Action(int next_state, int prediction) {
		super();
		this.next_state = next_state;
		this.prediction = prediction;
	}

	public int getNext_state() {
		return next_state;
	}

	public void setNext_state(int next_state) {
		this.next_state = next_state;
	}

	public int getPrediction() {
		return prediction;
	}

	public void setPrediction(int prediction) {
		this.prediction = prediction;
	}
	
	
}
