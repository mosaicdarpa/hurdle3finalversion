package com.uantwerpen.SC2.hurdle3.agent;

import com.uantwerpen.SC2.hurdle3.SolutionHandler;

public class StateActionHandler implements com.uantwerpen.valueIteration.StateActionHandler<State, Action> {

	private final int amount;
	
	public StateActionHandler() {
		amount = SolutionHandler.num_states * SolutionHandler.num_states;
	}

	@Override
	public int amountOfStates() {
		return amount;
	}

	@Override
	public int amountOfActions() {
		return amount;
	}

	@Override
	public State createState(int number) {
		int darpaChannel = number % SolutionHandler.num_states;
		int agentChannel = number / SolutionHandler.num_states;
		return new State(darpaChannel, agentChannel);
	}

	@Override
	public int getNumberState(State state) {
		return state.getDapraChannel() + state.getAgentChannel() * SolutionHandler.num_states;
	}

	@Override
	public Action createAction(int number) {
		int next_state = number % SolutionHandler.num_states;
		int prediction = number / SolutionHandler.num_states;
		return new Action(next_state, prediction);
	}

	@Override
	public int getNumberAction(Action action) {
		return action.getNext_state() + action.getPrediction() * SolutionHandler.num_states;
	}


}
