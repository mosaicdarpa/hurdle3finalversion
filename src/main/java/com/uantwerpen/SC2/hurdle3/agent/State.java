package com.uantwerpen.SC2.hurdle3.agent;

public class State implements com.uantwerpen.valueIteration.State {

	private int dapraChannel;
	private int agentChannel;
	
	public State(int dapraChannel, int agentChannel) {
		super();
		this.dapraChannel = dapraChannel;
		this.agentChannel = agentChannel;
	}

	public final int getDapraChannel() {
		return dapraChannel;
	}

	public final void setDapraChannel(int dapraChannel) {
		this.dapraChannel = dapraChannel;
	}

	public final int getAgentChannel() {
		return agentChannel;
	}

	public final void setAgentChannel(int agentChannel) {
		this.agentChannel = agentChannel;
	}
}
