package com.uantwerpen.SC2.hurdle3.agent;

import java.util.List;
import java.util.Random;

import com.uantwerpen.SC2.MDP.DynamicMDPModel;
import com.uantwerpen.SC2.hurdle3.Solution;
import com.uantwerpen.SC2.hurdle3.SolutionHandler;
import com.uantwerpen.SC2.hurdle3.thrift.StepResult;
import com.uantwerpen.valueIteration.ProbabilityFunction;
import com.uantwerpen.valueIteration.RewardFunction;
import com.uantwerpen.valueIteration.ValueIterationSolver;

public class ValueIterationAgent implements Solution {

	private StateActionHandler		handler;
	private Random					r;
	private int[]					policy;
	private DynamicMDPModel			model;
	private int						previousObservation;
	
	private List<List<List<Double>>> transitionMatrix;
	
	final private int				batchSize;
	
	public ValueIterationAgent() {
		handler = new StateActionHandler();
		batchSize = SolutionHandler.learningSteps / SolutionHandler.num_states;
	}

	@Override
	public StepResult start() {
		transitionMatrix = null;
		model = new DynamicMDPModel(SolutionHandler.num_states, SolutionHandler.num_states);
		r = new Random();
		previousObservation = r.nextInt(SolutionHandler.num_states);
		int prediction = r.nextInt(SolutionHandler.num_states);
		return new StepResult(prediction, 0);
	}

	@Override
	public StepResult step(int reward, int observation, int stepNr, StepResult previousStep) {
		if(stepNr < SolutionHandler.learningSteps){
			model.updateTransition(previousObservation, previousStep.next_state, observation);
			int next_state = stepNr / batchSize;
			previousObservation = observation;
			int prediction = r.nextInt(SolutionHandler.num_states);
			return new StepResult(prediction, next_state);
		}else if(stepNr == SolutionHandler.learningSteps){
			transitionMatrix = model.asArray();
			learn();
		}
		Action action = handler.createAction(policy[handler.getNumberState(new State(observation, previousStep.next_state))]);
		return new StepResult(action.getPrediction(), action.getNext_state());
	}

	private void learn(){
		ValueIterationSolver<State, Action> vi = new ValueIterationSolver<>(
				handler, 
				new RewardFunction<State, Action>() {

					@Override
					public double reward(State state0, Action action, State state1) {
						return (action.getNext_state() != state1.getDapraChannel() ? SolutionHandler.no_collision_reward : SolutionHandler.collision_penalty)
								+ (action.getPrediction() == state1.getDapraChannel() ? SolutionHandler.prediction_success_reward : 0);
					}
				}, new ProbabilityFunction<State, Action>() {

					@Override
					public double P(State state0, Action action, State state1) {
						if(action.getNext_state() == state1.getAgentChannel()){
							return transitionMatrix.get(state0.getDapraChannel()).get(state0.getAgentChannel()).get(state1.getDapraChannel());
						}
						return 0;
					}
				},
				0.9,			//GAMMA
				0.0000001,		//EPSILON
				10000);			//maxIterations
		policy = vi.getPolicy();
	}
}
