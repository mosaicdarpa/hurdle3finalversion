package com.uantwerpen.valueIteration;

import java.util.Random;
import java.util.function.IntConsumer;
import java.util.function.IntToDoubleFunction;
import java.util.stream.IntStream;

public class ValueIterationSolver<S extends State, A extends Action> {
	
	private StateActionHandler<S, A>	handler;
	private RewardFunction<S, A> 		rewardFunction;
	private ProbabilityFunction<S, A> 	probabilityFunction;
	private double						gamma;
	private double						epsilon;
	private long						maxIterations;
	
	public ValueIterationSolver(StateActionHandler<S, A> handler, 
			RewardFunction<S, A> rewardFunction,
			ProbabilityFunction<S, A> probabilityFunction,
			double gamma,
			double epsilon,
			long maxIterations) {
		this.handler = handler;
		this.rewardFunction = rewardFunction;
		this.probabilityFunction = probabilityFunction;
		this.gamma = gamma;
		this.epsilon = epsilon;
		this.maxIterations = maxIterations;
	}
	
	public int[] getPolicy(){
		double[] V = new double[handler.amountOfStates()];
		Random r = new Random();
		IntStream.range(0, handler.amountOfStates()).forEach(i -> V[i] = r.nextInt(handler.amountOfActions()));
		double delta = 0;
		long i = 0;
		do{
			delta = IntStream.range(0, handler.amountOfStates()).parallel().mapToDouble(new IntToDoubleFunction() {
				
				@Override
				public double applyAsDouble(int sStart) {
					S s0 = handler.createState(sStart);
					double v = V[sStart];
					double max = 0;
					for(int a = 0; a < handler.amountOfActions(); ++a){
						A a0 = handler.createAction(a);
						double sum = 0;
						for(int sEnd = 0; sEnd < handler.amountOfStates(); ++sEnd){
							S s1 = handler.createState(sEnd);
							sum += probabilityFunction.P(s0, a0, s1) * 
									(rewardFunction.reward(s0, a0, s1) + gamma * V[sEnd]);
						}
						max = sum > max ? sum : max;
					}
					V[sStart] = max;
					double d = Math.abs(v - max);
					return d;
				}
			}).max().getAsDouble();
			++i;
		}while(delta >= epsilon && i < maxIterations);
		System.out.format("iterations %d, maxIterations %d, delta %f\n", i, maxIterations, delta);
		
		int[] policy = new int[handler.amountOfStates()];
		IntStream.range(0, handler.amountOfStates()).parallel().forEach(new IntConsumer() {
			
			@Override
			public void accept(int s) {
				S s0 = handler.createState(s);
				int bestAction = 0;
				double maxValue = 0;
				for(int action = 0; action < handler.amountOfActions(); ++action){
					A a0 = handler.createAction(action);
					double sum = 0;
					for(int sEnd = 0; sEnd < handler.amountOfStates(); ++sEnd){
						S s1 = handler.createState(sEnd);
						sum += probabilityFunction.P(s0, a0, s1) * 
								(rewardFunction.reward(s0, a0, s1) + gamma * V[sEnd]);
					}
					if(sum > maxValue){
						bestAction = action;
						maxValue = sum;
					}
				}
				policy[s] = bestAction;
			}
		});
		
		return policy;
	}

}
