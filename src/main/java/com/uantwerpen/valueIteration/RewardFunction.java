package com.uantwerpen.valueIteration;

public interface RewardFunction<S extends State, A extends Action> {

	public double reward(S state0, A action, S state1);
	
}
