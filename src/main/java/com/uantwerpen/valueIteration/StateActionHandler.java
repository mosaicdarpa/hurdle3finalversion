package com.uantwerpen.valueIteration;

public interface StateActionHandler<S extends State, A extends Action> {

	public int amountOfStates();
	
	public int amountOfActions();
	
	public S createState(int number);
	
	public int getNumberState(S state);
	
	public A createAction(int number);
	
	public int getNumberAction(A action);
	
}
