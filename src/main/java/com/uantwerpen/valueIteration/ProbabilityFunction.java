package com.uantwerpen.valueIteration;

public interface ProbabilityFunction<S extends State, A extends Action> {

	public double P(S state0, A action, S state1);
	
}
